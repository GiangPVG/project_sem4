﻿using Shoes.Data.Infrastructure;
using Shoes.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shoes.Data.Repositories
{
    public interface IMenuGroupRepository: IRepository<MenuGroup>
    {

    }
    public class MenuGroupRepository : RepositoryBase<MenuGroup>, IMenuGroupRepository
    {
        protected MenuGroupRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
