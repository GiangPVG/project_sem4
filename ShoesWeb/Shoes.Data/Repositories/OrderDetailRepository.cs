﻿using Shoes.Data.Infrastructure;
using Shoes.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shoes.Data.Repositories
{
    public interface IOrderDetailRepository: IRepository<OrderDetail>
    {

    }
    public class OrderDetailRepository : RepositoryBase<OrderDetail>, IOrderDetailRepository
    {
        protected OrderDetailRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
