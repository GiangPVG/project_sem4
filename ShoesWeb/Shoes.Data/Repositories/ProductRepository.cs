﻿using Shoes.Data.Infrastructure;
using Shoes.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shoes.Data.Repositories
{
    public interface IProductRepository:IRepository<Product>
    {

    }
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        protected ProductRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
