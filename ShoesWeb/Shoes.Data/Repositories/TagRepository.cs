﻿using Shoes.Data.Infrastructure;
using Shoes.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shoes.Data.Repositories
{
    public interface ITagRepository: IRepository<Tag>
    {


    }
    public class TagRepository : RepositoryBase<Tag>, ITagRepository
    {
        protected TagRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
