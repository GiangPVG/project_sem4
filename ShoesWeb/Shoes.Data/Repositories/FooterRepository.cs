﻿using Shoes.Data.Infrastructure;
using Shoes.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shoes.Data.Repositories
{
    public interface IFooterRepository : IRepository<Footer>
    {

    }

    public class FooterRepository : RepositoryBase<Footer>, IFooterRepository
    {
        protected FooterRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
